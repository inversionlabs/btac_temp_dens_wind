#!/usr/bin/env python

# Script to analyze air temp, density, and wind data
# Patrick Wright, Inversion Labs
# Job: BTAC
# January, 2016

import numpy as np
import scipy as sp
from scipy import stats
import scipy.stats as stats
import pylab 
import pandas as pd
from IPython import embed
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import statsmodels.api as sm
from statsmodels.sandbox.regression.predstd import wls_prediction_std
import statsmodels.formula.api as smf
import datetime as dt

# --------------------------------------------------------------------------
def histoplot(data,titlestring):
	num_bins=(2060 / 10.)
	fig10 = plt.figure(figsize=(10,7))
	ax = fig10.add_subplot(111)
	n, bins, patches = plt.hist(data, num_bins, normed=1, facecolor='blue', alpha = 0.5)
	#plt.axvline(x=np.median(data), ymin=0, ymax=180, linewidth=1, color='r')
	#plt.axvline(x=np.mean(data), ymin=0, ymax=180, linewidth=1, color='k')
	ax.set_title(titlestring)
	ax.set_xlabel('density')
	ax.set_ylabel('# measurements / 10')

# --------------------------------------------------------------------------
print "Processing datafiles..."

# Define directory paths:
fpath_1='historic_20160124.csv' # historical data
fpath_2='recent_20160124.csv' # since 2002

# Read in data:
data1 = pd.read_csv(fpath_1, sep=',')
data2 = pd.read_csv(fpath_2, sep=',')

# Convert datetime strings to a Pandas datetime object
timestamp1 = data1['fldDate']
dt1 = pd.to_datetime(timestamp1, infer_datetime_format=True)
timestamp2 = data2['fldDate']
dt2 = pd.to_datetime(timestamp2, infer_datetime_format=True)

# set index of dataframes to datetime
data1_dt = data1.set_index(dt1)
data2_dt = data2.set_index(dt2)

# get rid of redundant date column:
data1_dt.drop('fldDate', axis=1, inplace=True)
data2_dt.drop('fldDate', axis=1, inplace=True)

# ---------------------------------------------
# WIND -- Total Miles Summit
# ---------------------------------------------
total_miles_historic = (data1_dt['UWind24_06Miles'] + data1_dt['UWind06_12Miles']
                        + data1_dt['UWind12_18Miles'] + data1_dt['UWind18_24Miles'])

total_miles_recent = data2_dt['TtlWindMilesSummit']

# BUILD CONTINUOUS WIND SERIES:
total_miles1 = total_miles_historic[:'2002-01-30']
total_miles2 = total_miles_recent

total_miles_list = [total_miles1, total_miles2]

total_miles = pd.concat(total_miles_list)

# Count NaNs in wind data:
NaNcount_wind = total_miles.isnull().sum()
fractNaN_wind = float(NaNcount_wind) / float(len(total_miles))

print '------------------------------'
print "%s NaN values in total_miles" %NaNcount_wind
print "%f of data" %fractNaN_wind 
print '------------------------------'

total_miles_resample = total_miles.resample('1D')

total_miles_resample = total_miles_resample['1974-12-16':'2016-01-21'] # common to last NewSnowMid entry

# ---------------------------------------------
# TEMP
# ---------------------------------------------

# MID:
# Grab current/max/min from data1
CurTemp1 = data1_dt.CurTempMid[:'2002-01-30']
MaxTemp1 = data1_dt.MaxTempMid[:'2002-01-30']
MinTemp1 = data1_dt.MinTempMid[:'2002-01-30']

# Grab current/max/min from data2
CurTemp2 = data2_dt.CurTempMid
MaxTemp2 = data2_dt.MaxTempMid
MinTemp2 = data2_dt.MinTempMid

# Define two corresponding series as a list
CurTemp_data = [CurTemp1, CurTemp2]
MaxTemp_data = [MaxTemp1, MaxTemp2]
MinTemp_data = [MinTemp1, MinTemp2]

# Concat to one continuous variable
CurTempMid = pd.concat(CurTemp_data)
MaxTempMid = pd.concat(MaxTemp_data)
MinTempMid = pd.concat(MinTemp_data)

# Count NaNs in temp data:
NaNcount_Cur = CurTempMid.isnull().sum()
fractNaN_Cur = float(NaNcount_Cur) / float(len(CurTempMid))
NaNcount_Max = MaxTempMid.isnull().sum()
fractNaN_Max = float(NaNcount_Max) / float(len(MaxTempMid))
NaNcount_Min = MinTempMid.isnull().sum()
fractNaN_Min = float(NaNcount_Min) / float(len(MinTempMid))

print '------------------------------'
print "%s NaN values in CurTempMid" %NaNcount_Cur
print "%f of data" %fractNaN_Cur 
print "%s NaN values in MaxTempMid" %NaNcount_Max
print "%f of data" %fractNaN_Max
print "%s NaN values in MinTempMid" %NaNcount_Min
print "%f of data" %fractNaN_Min 
print '------------------------------'

# RESAMPLE (runs through summers)
CurTempMid_resample = CurTempMid.resample('1D')
MaxTempMid_resample = MaxTempMid.resample('1D')
MinTempMid_resample = MinTempMid.resample('1D')

CurTempMid_resample = CurTempMid_resample['1974-12-16':'2016-01-21']
MaxTempMid_resample = MaxTempMid_resample['1974-12-16':'2016-01-21']
MinTempMid_resample = MinTempMid_resample['1974-12-16':'2016-01-21']

# Calc average temp:
temp_mean_mid = (MaxTempMid_resample + MinTempMid_resample) / 2.

#---------------------------------------------------------------------

# RAYMER:
# Grab current/max/min from data1
CurTemp1 = data1_dt.CurTempRay[:'2002-01-30']
MaxTemp1 = data1_dt.MaxTempRay[:'2002-01-30']
MinTemp1 = data1_dt.MinTempRay[:'2002-01-30']

# Grab current/max/min from data2
CurTemp2 = data2_dt.CurTempRay
MaxTemp2 = data2_dt.MaxTempRay
MinTemp2 = data2_dt.MinTempRay

# Define two corresponding series as a list
CurTemp_data = [CurTemp1, CurTemp2]
MaxTemp_data = [MaxTemp1, MaxTemp2]
MinTemp_data = [MinTemp1, MinTemp2]

# Concat to one continuous variable
CurTempRay = pd.concat(CurTemp_data)
MaxTempRay = pd.concat(MaxTemp_data)
MinTempRay = pd.concat(MinTemp_data)

# Count NaNs in temp data:
NaNcount_Cur = CurTempRay.isnull().sum()
fractNaN_Cur = float(NaNcount_Cur) / float(len(CurTempRay))
NaNcount_Max = MaxTempRay.isnull().sum()
fractNaN_Max = float(NaNcount_Max) / float(len(MaxTempRay))
NaNcount_Min = MinTempRay.isnull().sum()
fractNaN_Min = float(NaNcount_Min) / float(len(MinTempRay))

print '------------------------------'
print "%s NaN values in CurTempRay" %NaNcount_Cur
print "%f of data" %fractNaN_Cur 
print "%s NaN values in MaxTempRay" %NaNcount_Max
print "%f of data" %fractNaN_Max
print "%s NaN values in MinTempRay" %NaNcount_Min
print "%f of data" %fractNaN_Min 
print '------------------------------'

# RESAMPLE (runs through summers)
CurTempRay_resample = CurTempRay.resample('1D')
MaxTempRay_resample = MaxTempRay.resample('1D')
MinTempRay_resample = MinTempRay.resample('1D')

CurTempRay_resample = CurTempRay_resample['1974-12-16':'2016-01-21']

MaxTempRay_resample = MaxTempRay_resample['1974-12-16':'2016-01-21']
MaxTempRay_resample[MaxTempRay_resample < -50.] = np.NaN
MaxTempRay_resample[MaxTempRay_resample > 100.] = np.NaN

MinTempRay_resample = MinTempRay_resample['1974-12-16':'2016-01-21']
MinTempRay_resample[MinTempRay_resample < -50.] = np.NaN

#embed()
# Calc average temp:
temp_mean_ray = (MaxTempRay_resample + MinTempRay_resample) / 2.

#---------------------------------------------------------------------

# SUMMIT:
# Grab current/max/min from data1
CurTemp1 = data1_dt.CurTempSummit[:'2002-01-30']
MaxTemp1 = data1_dt.MaxTempSummit[:'2002-01-30']
MinTemp1 = data1_dt.MinTempSummit[:'2002-01-30']

# Grab current/max/min from data2
CurTemp2 = data2_dt.CurTempSummit
MaxTemp2 = data2_dt.MaxTempSummit
MinTemp2 = data2_dt.MinTempSummit

# Define two corresponding series as a list
CurTemp_data = [CurTemp1, CurTemp2]
MaxTemp_data = [MaxTemp1, MaxTemp2]
MinTemp_data = [MinTemp1, MinTemp2]

# Concat to one continuous variable
CurTempSummit = pd.concat(CurTemp_data)
MaxTempSummit = pd.concat(MaxTemp_data)
MinTempSummit = pd.concat(MinTemp_data)

# Count NaNs in temp data:
NaNcount_Cur = CurTempSummit.isnull().sum()
fractNaN_Cur = float(NaNcount_Cur) / float(len(CurTempSummit))
NaNcount_Max = MaxTempSummit.isnull().sum()
fractNaN_Max = float(NaNcount_Max) / float(len(MaxTempSummit))
NaNcount_Min = MinTempSummit.isnull().sum()
fractNaN_Min = float(NaNcount_Min) / float(len(MinTempSummit))

print '------------------------------'
print "%s NaN values in CurTempSummit" %NaNcount_Cur
print "%f of data" %fractNaN_Cur 
print "%s NaN values in MaxTempSummit" %NaNcount_Max
print "%f of data" %fractNaN_Max
print "%s NaN values in MinTempSummit" %NaNcount_Min
print "%f of data" %fractNaN_Min 
print '------------------------------'

# RESAMPLE (runs through summers)
CurTempSummit_resample = CurTempSummit.resample('1D')
MaxTempSummit_resample = MaxTempSummit.resample('1D')
MinTempSummit_resample = MinTempSummit.resample('1D')

CurTempSummit_resample = CurTempSummit_resample['1974-12-16':'2016-01-21']
MaxTempSummit_resample = MaxTempSummit_resample['1974-12-16':'2016-01-21']
MaxTempSummit_resample[MaxTempSummit_resample < -50.] = np.NaN
MaxTempSummit_resample[MaxTempSummit_resample > 100.] = np.NaN

MinTempSummit_resample = MinTempSummit_resample['1974-12-16':'2016-01-21']
MinTempSummit_resample[MinTempSummit_resample < -50.] = np.NaN


# Calc average temp:
temp_mean_Summit = (MaxTempSummit_resample + MinTempSummit_resample) / 2.

#embed()

# ---------------------------------------------
# SNOW
# ---------------------------------------------

# MID
# Count initial NaNs in snow data:
NaNcount_newsnow = data1_dt.NewSnowMid.isnull().sum()
fractNaN_newsnow = float(NaNcount_newsnow) / float(len(data1_dt.NewSnowMid))
NaNcount_SWE = data1_dt.SWEMid.isnull().sum()
fractNaN_SWE = float(NaNcount_SWE) / float(len(data1_dt.SWEMid))

print '------------------------------'
print "%s NaN values in NewSnowMid" %NaNcount_newsnow
print "%f of data" %fractNaN_newsnow 
print "%s NaN values in SWEMid" %NaNcount_SWE
print "%f of data" %fractNaN_SWE
print '------------------------------'

print 'Dropping any new snow entries <=1"'

NewSnowMid_countzero = data1_dt.NewSnowMid[data1_dt.NewSnowMid == 0.0]
NewSnowMid_drop = data1_dt.NewSnowMid[data1_dt.NewSnowMid > 1.0]
#NewSnowMid_drop = data1_dt.NewSnowMid[data1_dt.NewSnowMid > 2.0]
#NewSnowMid_drop = data1_dt.NewSnowMid[data1_dt.NewSnowMid > 3.0]

lessthaninch = len(data1_dt.NewSnowMid) - len(NewSnowMid_drop) - len(NewSnowMid_countzero)
totaldrop = len(data1_dt.NewSnowMid) - len(NewSnowMid_drop)

print "%s entries in NewSnowMid, >0 <=1" %lessthaninch
#print "%s entries in NewSnowMid, >0 <=2" %lessthaninch
#print "%s entries in NewSnowMid, >0 <=3" %lessthaninch
print "dropped %s entries from NewSnowMid (including 0)" %totaldrop

# Match length with temp data
NewSnowMid_resample = NewSnowMid_drop.resample('1D')
SWEMid_resample = data1_dt.SWEMid.resample('1D')

NewSnowMid_resample = NewSnowMid_resample['1974-12-16':'2016-01-21']
SWEMid_resample = SWEMid_resample['1974-12-16':'2016-01-21']

densityMid = SWEMid_resample / NewSnowMid_resample 
# any NaNs in NewSnowMid_resample should set corresponding densityMid to NaN

print 'Dropping outlier high density records'
densityMid = densityMid[densityMid < 0.25]

#------------------------------------------------------------------------------

# BOWL:
# Count initial NaNs in snow data:
NaNcount_newsnow = data1_dt.NewSnowBowl.isnull().sum()
fractNaN_newsnow = float(NaNcount_newsnow) / float(len(data1_dt.NewSnowBowl))
NaNcount_SWE = data1_dt.SWEBowl.isnull().sum()
fractNaN_SWE = float(NaNcount_SWE) / float(len(data1_dt.SWEBowl))

print '------------------------------'
print "%s NaN values in NewSnowBowl" %NaNcount_newsnow
print "%f of data" %fractNaN_newsnow 
print "%s NaN values in SWEBowl" %NaNcount_SWE
print "%f of data" %fractNaN_SWE
print '------------------------------'

print 'Dropping any new snow entries <=1"'

NewSnowBowl_countzero = data1_dt.NewSnowBowl[data1_dt.NewSnowBowl == 0.0]
NewSnowBowl_drop = data1_dt.NewSnowBowl[data1_dt.NewSnowBowl > 1.0]
#NewSnowBowl_drop = data1_dt.NewSnowBowl[data1_dt.NewSnowBowl > 2.0]
#NewSnowBowl_drop = data1_dt.NewSnowBowl[data1_dt.NewSnowBowl > 3.0]

lessthaninch = len(data1_dt.NewSnowBowl) - len(NewSnowBowl_drop) - len(NewSnowBowl_countzero)
totaldrop = len(data1_dt.NewSnowBowl) - len(NewSnowBowl_drop)

print "%s entries in NewSnowBowl, >0 <=1" %lessthaninch
#print "%s entries in NewSnowBowl, >0 <=2" %lessthaninch
#print "%s entries in NewSnowBowl, >0 <=3" %lessthaninch
print "dropped %s entries from NewSnowBowl (including 0)" %totaldrop

# Match length with temp data
NewSnowBowl_resample = NewSnowBowl_drop.resample('1D')
SWEBowl_resample = data1_dt.SWEBowl.resample('1D')

NewSnowBowl_resample = NewSnowBowl_resample['1974-12-16':'2016-01-21']
SWEBowl_resample = SWEBowl_resample['1974-12-16':'2016-01-21']

densityBowl = SWEBowl_resample / NewSnowBowl_resample 
# any NaNs in NewSnowBowl_resample should set corresponding densityMid to NaN

print 'Dropping outlier high density records'
densityBowl = densityBowl[densityBowl < 0.25]

#------------------------------------------------------------------------------

# BRIDGER:
# Count initial NaNs in snow data:
NaNcount_newsnow = data1_dt.NewSnowBridger.isnull().sum()
fractNaN_newsnow = float(NaNcount_newsnow) / float(len(data1_dt.NewSnowBridger))
NaNcount_SWE = data1_dt.SWEBridger.isnull().sum()
fractNaN_SWE = float(NaNcount_SWE) / float(len(data1_dt.SWEBridger))

print '------------------------------'
print "%s NaN values in NewSnowBridger" %NaNcount_newsnow
print "%f of data" %fractNaN_newsnow 
print "%s NaN values in SWEBridger" %NaNcount_SWE
print "%f of data" %fractNaN_SWE
print '------------------------------'

print 'Dropping any new snow entries <=1"'

NewSnowBridger_countzero = data1_dt.NewSnowBridger[data1_dt.NewSnowBridger == 0.0]
NewSnowBridger_drop = data1_dt.NewSnowBridger[data1_dt.NewSnowBridger > 1.0]
#NewSnowBridger_drop = data1_dt.NewSnowBridger[data1_dt.NewSnowBridger > 2.0]
#NewSnowBridger_drop = data1_dt.NewSnowBridger[data1_dt.NewSnowBridger > 3.0]

lessthaninch = len(data1_dt.NewSnowBridger) - len(NewSnowBridger_drop) - len(NewSnowBridger_countzero)
totaldrop = len(data1_dt.NewSnowBridger) - len(NewSnowBridger_drop)

print "%s entries in NewSnowBridger, >0 <=1" %lessthaninch
#print "%s entries in NewSnowBridger, >0 <=2" %lessthaninch
#print "%s entries in NewSnowBridger, >0 <=3" %lessthaninch
print "dropped %s entries from NewSnowBridger (including 0)" %totaldrop

# Match length with temp data
NewSnowBridger_resample = NewSnowBridger_drop.resample('1D')
SWEBridger_resample = data1_dt.SWEBridger.resample('1D')

NewSnowBridger_resample = NewSnowBridger_resample['1974-12-16':'2016-01-21']
SWEBridger_resample = SWEBridger_resample['1974-12-16':'2016-01-21']

densityBridger = SWEBridger_resample / NewSnowBridger_resample 
# any NaNs in NewSnowBridger_resample should set corresponding densityMid to NaN

print 'Dropping outlier high density records'
densityBridger = densityBridger[densityBridger < 0.25]

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------

# Only select values for common integers (non-NaN):
idx_mean_mid = np.isfinite(temp_mean_mid) & np.isfinite(densityMid) & np.isfinite(total_miles_resample)

idx_mean_bowl = np.isfinite(temp_mean_Summit) & np.isfinite(densityBowl) & np.isfinite(total_miles_resample)

idx_mean_bridger = np.isfinite(temp_mean_ray) & np.isfinite(densityBridger) & np.isfinite(total_miles_resample)

#idx_mean_mid = idx_mean_mid[idx_mean_mid==True]
#idx_mean_bowl = idx_mean_bowl[idx_mean_bowl==True]
#idx_mean_bridger = idx_mean_bridger[idx_mean_bridger==True]

# Limit dates to Dec. 15 - March 30:
idx_mean_mid_tight = idx_mean_mid[((idx_mean_mid.index.month == 12) & (15 <= idx_mean_mid.index.day)  # Dec after the 15th inclusive
              | (idx_mean_mid.index.month == 1) 
              | (idx_mean_mid.index.month == 2) 
              | (idx_mean_mid.index.month == 3) & (idx_mean_mid.index.day <= 30 ))]  # March before the 30th inclusive 

idx_mean_bowl_tight = idx_mean_bowl[((idx_mean_bowl.index.month == 12) & (15 <= idx_mean_bowl.index.day)  # Dec after the 15th inclusive
              | (idx_mean_mid.index.month == 1) 
              | (idx_mean_mid.index.month == 2) 
              | (idx_mean_bowl.index.month == 3) & (idx_mean_bowl.index.day <= 30 ))]  # March before the 30th inclusive 

idx_mean_bridger_tight = idx_mean_bridger[((idx_mean_bridger.index.month == 12) & (15 <= idx_mean_bridger.index.day)  # Dec after the 15th inclusive
              | (idx_mean_mid.index.month == 1) 
              | (idx_mean_mid.index.month == 2) 
              | (idx_mean_bridger.index.month == 3) & (idx_mean_bridger.index.day <= 30 ))]  # March before the 30th inclusive 

idx_mid = idx_mean_mid_tight[idx_mean_mid_tight==True]
idx_bowl = idx_mean_bowl_tight[idx_mean_bowl_tight==True]
idx_bridger = idx_mean_bridger_tight[idx_mean_bridger_tight==True]

print ''
print '-----------------------'
print '|  MID: n = %d      |' % len(idx_mid[idx_mid==True])
print '|  BOWL: n = %d     |' % len(idx_bowl[idx_bowl==True])
print '|  BRIDGER: n = %d   |' % len(idx_bridger[idx_bridger==True])
print '-----------------------'
print ''

#embed()

#--------------------------------------------------------------------------------------
# 3D PLOTS
#--------------------------------------------------------------------------------------

#from mpl_toolkits.mplot3d import Axes3D
#import scipy.linalg
#from matplotlib import cm

## MID

#x = temp_mean_mid[idx_mid.index]
#x = (x - 32.) * (5./9.)

#y = total_miles_resample[idx_mid.index]
#y = y * 1.60934

#z = densityMid[idx_mid.index]

##histoplot(z,'MID (Dec. 15 - March 30)')

#d = {'temp': x, 'wind': y, 'dens': z}
#df = pd.DataFrame(data=d)

#x = df.temp
#y = df.wind
#z = df.dens

## 3D Scatter
##fig = plt.figure()
##ax = fig.add_subplot(111, projection='3d')
##ax.scatter(x, y, z, c='b', marker='o')
##ax.set_xlabel('MID mean temp (f)')
##ax.set_ylabel('SUMMIT total wind miles')
##ax.set_zlabel('MID density')
##ax.set_title('MID')


## Add best-fit 2D plane:

#data = np.c_[x,y,z]

## regular grid covering the domain of the data
#mn = np.min(data, axis=0)
#mx = np.max(data, axis=0)
#X,Y = np.meshgrid(np.linspace(mn[0], mx[0], 20), np.linspace(mn[1], mx[1], 20))
#XX = X.flatten()
#YY = Y.flatten()

#order = 2    # 1: linear, 2: quadratic
#if order == 1:
    ## best-fit linear plane
    #A = np.c_[data[:,0], data[:,1], np.ones(data.shape[0])]
    #C,_,_,_ = scipy.linalg.lstsq(A, data[:,2])    # coefficients
    
    ## evaluate it on grid
    #Z = C[0]*X + C[1]*Y + C[2]
    
    ## or expressed using matrix/vector product
    ##Z = np.dot(np.c_[XX, YY, np.ones(XX.shape)], C).reshape(X.shape)

#elif order == 2:
    ## best-fit quadratic curve
    #A = np.c_[np.ones(data.shape[0]), data[:,:2], np.prod(data[:,:2], axis=1), data[:,:2]**2]
    #C,_,_,_ = scipy.linalg.lstsq(A, data[:,2])
    
    ## evaluate it on a grid
    #Z = np.dot(np.c_[np.ones(XX.shape), XX, YY, XX*YY, XX**2, YY**2], C).reshape(X.shape)


#def calc_dens_2(temp,wind):
  #density = C[4]*temp**2. + C[5]*wind**2. + C[3]*temp*wind + C[1]*temp + C[2]*wind + C[0]
  #return density

#print ''
#print '----------------------------------------------'
#print 'MID coefficients:'
#print C
#print '----------------------------------------------'
#print ''

#density_2 = calc_dens_2(35,1000)

#print ' '
#print "-----------MID-----------"
#est = smf.ols(formula='dens ~ 1 + I(temp ** 2.0) + I(wind ** 2.0) + I(temp * wind) + temp + wind', data=df).fit()
#print est.summary()

## plot points and fitted surface
#fig = plt.figure(figsize=(11.5,9))
#ax = fig.gca(projection='3d')

#surf = ax.plot_surface(X, Y, Z, rstride=50, cstride=50, alpha=0.2)
#ax.scatter(data[:,0], data[:,1], data[:,2], c='r', s=50, alpha=0.3)

##ax.scatter(35,1000,density_2, c='r', s=100)

#plt.xlabel('Mean 24-hr Air Temperature ($^\circ$C) (Mid)', fontsize=14, labelpad=11)
#plt.ylabel('Total 24-hr Wind Kilometers (Summit)', fontsize=14, labelpad=14)
#ax.set_zlabel('Density (Mid)', fontsize=14, labelpad=17)
#ax.set_title('')

#for tick in ax.xaxis.get_major_ticks():
                #tick.label.set_fontsize(14)
                #tick.label1.set_horizontalalignment('right')
#for tick in ax.yaxis.get_major_ticks():
                #tick.label.set_fontsize(14)
                #tick.label1.set_horizontalalignment('left')
#for tick in ax.zaxis.get_major_ticks():
                #tick.label.set_fontsize(14)
                
#ax.tick_params(axis='x', which='major', pad=-1)
#ax.tick_params(axis='y', which='major', pad=-1)
#ax.tick_params(axis='z', which='major', pad=9)

#ax.axis('equal')
#ax.axis('tight')

#plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.00)
#fig.tight_layout(pad=0.5)

##plt.savefig('3D_mid_450.png',dpi=450)
#plt.savefig('3D_mid_450_metric.png',dpi=450)

##plt.show()

##---------------------------------------------------------------------------------
## BOWL

#x = temp_mean_Summit[idx_bowl.index]
#y = total_miles_resample[idx_bowl.index]
#z = densityBowl[idx_bowl.index]

#histoplot(z,'BOWL (Dec. 15 - March 30)')

#d = {'temp': x, 'wind': y, 'dens': z}
#df = pd.DataFrame(data=d)

#x = df.temp
#y = df.wind
#z = df.dens

## 3D Scatter
##fig = plt.figure()
##ax = fig.add_subplot(111, projection='3d')
##ax.scatter(x, y, z, c='b', marker='o')
##ax.set_xlabel('MID mean temp (f)')
##ax.set_ylabel('SUMMIT total wind miles')
##ax.set_zlabel('BOWL density')
##ax.set_title('BOWL')


## Add best-fit 2D plane:

#data = np.c_[x,y,z]

## regular grid covering the domain of the data
#mn = np.min(data, axis=0)
#mx = np.max(data, axis=0)
#X,Y = np.meshgrid(np.linspace(mn[0], mx[0], 20), np.linspace(mn[1], mx[1], 20))
#XX = X.flatten()
#YY = Y.flatten()

#order = 2    # 1: linear, 2: quadratic
#if order == 1:
    ## best-fit linear plane
    #A = np.c_[data[:,0], data[:,1], np.ones(data.shape[0])]
    #C,_,_,_ = scipy.linalg.lstsq(A, data[:,2])    # coefficients
    
    ## evaluate it on grid
    #Z = C[0]*X + C[1]*Y + C[2]
    
    ## or expressed using matrix/vector product
    ##Z = np.dot(np.c_[XX, YY, np.ones(XX.shape)], C).reshape(X.shape)

#elif order == 2:
    ## best-fit quadratic curve
    #A = np.c_[np.ones(data.shape[0]), data[:,:2], np.prod(data[:,:2], axis=1), data[:,:2]**2]
    #C,_,_,_ = scipy.linalg.lstsq(A, data[:,2])
    
    ## evaluate it on a grid
    #Z = np.dot(np.c_[np.ones(XX.shape), XX, YY, XX*YY, XX**2, YY**2], C).reshape(X.shape)

#print ''
#print '----------------------------------------------'
#print 'BOWL coefficients:'
#print C
#print '----------------------------------------------'
#print ''

#print ' '
#print "-----------BOWL-----------"
#est = smf.ols(formula='dens ~ 1 + I(temp ** 2.0) + I(wind ** 2.0) + I(temp * wind) + temp + wind', data=df).fit()
#print est.summary()

##embed()

## plot points and fitted surface
#fig = plt.figure()
#ax = fig.gca(projection='3d')

#ax.plot_surface(X, Y, Z, rstride=50, cstride=50, alpha=0.2)
#ax.scatter(data[:,0], data[:,1], data[:,2], c='r', s=50, alpha=0.3)

#plt.xlabel('SUMMIT (upper) Mean temp (F)')
#plt.ylabel('SUMMIT Total wind miles')
#ax.set_zlabel('BOWL Density')
#ax.set_title('BOWL')
#ax.axis('equal')
#ax.axis('tight')

##plt.show()

##---------------------------------------------------------------------------------
## BRIDGER

#x = temp_mean_ray[idx_bridger.index]
#y = total_miles_resample[idx_bridger.index]
#z = densityBridger[idx_bridger.index]

#d = {'temp': x, 'wind': y, 'dens': z}
#df = pd.DataFrame(data=d)

#x = df.temp
#y = df.wind
#z = df.dens

#histoplot(z,'BRIDGER (Dec. 15 - March 30')

## 3D Scatter
##fig = plt.figure()
##ax = fig.add_subplot(111, projection='3d')
##ax.scatter(x, y, z, c='b', marker='o')
##ax.set_xlabel('MID mean temp (f)')
##ax.set_ylabel('SUMMIT total wind miles')
##ax.set_zlabel('BRIDGER density')
##ax.set_title('BRIDGER')


## Add best-fit 2D plane:

#data = np.c_[x,y,z]

## regular grid covering the domain of the data
#mn = np.min(data, axis=0)
#mx = np.max(data, axis=0)
#X,Y = np.meshgrid(np.linspace(mn[0], mx[0], 20), np.linspace(mn[1], mx[1], 20))
#XX = X.flatten()
#YY = Y.flatten()

#order = 2    # 1: linear, 2: quadratic
#if order == 1:
    ## best-fit linear plane
    #A = np.c_[data[:,0], data[:,1], np.ones(data.shape[0])]
    #C,_,_,_ = scipy.linalg.lstsq(A, data[:,2])    # coefficients
    
    ## evaluate it on grid
    #Z = C[0]*X + C[1]*Y + C[2]
    
    ## or expressed using matrix/vector product
    ##Z = np.dot(np.c_[XX, YY, np.ones(XX.shape)], C).reshape(X.shape)

#elif order == 2:
    ## best-fit quadratic curve
    #A = np.c_[np.ones(data.shape[0]), data[:,:2], np.prod(data[:,:2], axis=1), data[:,:2]**2]
    #C,_,_,_ = scipy.linalg.lstsq(A, data[:,2])
    
    ## evaluate it on a grid
    #Z = np.dot(np.c_[np.ones(XX.shape), XX, YY, XX*YY, XX**2, YY**2], C).reshape(X.shape)
    
#print ''
#print '----------------------------------------------'
#print 'BRIDGER coefficients:'
#print C
#print '----------------------------------------------'
#print ''

#print ' '
#print "-----------BRIDGER-----------"
#est = smf.ols(formula='dens ~ 1 + I(temp ** 2.0) + I(wind ** 2.0) + I(temp * wind) + temp + wind', data=df).fit()
#print est.summary()

##embed()

## plot points and fitted surface
#fig = plt.figure()
#ax = fig.gca(projection='3d')

#ax.plot_surface(X, Y, Z, rstride=50, cstride=50, alpha=0.2)
#ax.scatter(data[:,0], data[:,1], data[:,2], c='r', s=50, alpha=0.3)

#plt.xlabel('RAYMER Mean temp (F)')
#plt.ylabel('SUMMIT Total wind miles')
#ax.set_zlabel('BRIDGER Density')
#ax.set_title('BRIDGER')
#ax.axis('equal')
#ax.axis('tight')

##----------------------------------------------------------------------------------
# WIND vs. DENSITY, basic 1st and 2nd order fits
##----------------------------------------------------------------------------------

## MID wind vs. density:
#x=total_miles_resample[idx_mid.index]
#y=densityMid[idx_mid.index]

#d = {'wind': x, 'dens': y}
#df = pd.DataFrame(data=d)

#plt.figure(figsize=(6 * 1.618, 6))
#plt.plot(x,y, 'o', alpha=0.2)
#plt.xlabel('total wind miles SUMMIT')
#plt.ylabel('density MID')
#plt.title('MID')

## points linearly spaced on temp
#x1 = pd.DataFrame({'wind': np.linspace(df.wind.min(), df.wind.max(), 100)})

## 1st order polynomial
#poly_1 = smf.ols(formula='dens ~ 1 + wind', data=df).fit()
#plt.plot(x1.wind, poly_1.predict(x1), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1.rsquared, 
         #alpha=0.9)

## 2nd order polynomial
#poly_2 = smf.ols(formula='dens ~ 1 + wind + I(wind ** 2.0)', data=df).fit()
#plt.plot(x1.wind, poly_2.predict(x1), 'g-', label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared, 
         #alpha=0.9)

#plt.legend()


## BOWL wind vs. density:
#x=total_miles_resample[idx_bowl.index]
#y=densityBowl[idx_bowl.index]

#d = {'wind': x, 'dens': y}
#df = pd.DataFrame(data=d)

#plt.figure(figsize=(6 * 1.618, 6))
#plt.plot(x,y, 'o', alpha=0.2)
#plt.xlabel('total wind miles SUMMIT')
#plt.ylabel('density BOWL')
#plt.title('BOWL')

## points linearly spaced on temp
#x1 = pd.DataFrame({'wind': np.linspace(df.wind.min(), df.wind.max(), 100)})

## 1st order polynomial
#poly_1 = smf.ols(formula='dens ~ 1 + wind', data=df).fit()
#plt.plot(x1.wind, poly_1.predict(x1), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1.rsquared, 
         #alpha=0.9)

## 2nd order polynomial
#poly_2 = smf.ols(formula='dens ~ 1 + wind + I(wind ** 2.0)', data=df).fit()
#plt.plot(x1.wind, poly_2.predict(x1), 'g-', label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared, 
         #alpha=0.9)

#plt.legend()


## BRIDGER wind vs. density:
#x=total_miles_resample[idx_bridger.index]
#y=densityBridger[idx_bridger.index]

#d = {'wind': x, 'dens': y}
#df = pd.DataFrame(data=d)

#plt.figure(figsize=(6 * 1.618, 6))
#plt.plot(x,y, 'o', alpha=0.2)
#plt.xlabel('total wind miles SUMMIT')
#plt.ylabel('density BRIDGER')
#plt.title('BRIDGER')

## points linearly spaced on temp
#x1 = pd.DataFrame({'wind': np.linspace(df.wind.min(), df.wind.max(), 100)})

## 1st order polynomial
#poly_1 = smf.ols(formula='dens ~ 1 + wind', data=df).fit()
#plt.plot(x1.wind, poly_1.predict(x1), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1.rsquared, 
         #alpha=0.9)

## 2nd order polynomial
#poly_2 = smf.ols(formula='dens ~ 1 + wind + I(wind ** 2.0)', data=df).fit()
#plt.plot(x1.wind, poly_2.predict(x1), 'g-', label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared, 
         #alpha=0.9)

#plt.legend()


# -----TURN ON EVERYTHING BELOW FOR WIND AND TEMP QR & CI/PI PLOTS-----

#-----------------------------------------------------------------------------
# WIND vs DENSITY: CI and PI for 2nd order poly
#-----------------------------------------------------------------------------
# adapted from: http://stackoverflow.com/questions/34998772/plotting-confidence-and-prediction-intervals-with-repeated-entries

#x=total_miles_resample[idx_mid.index]

## Toggle below for metrics!
#x = x * 1.60934

#y=densityMid[idx_mid.index]

#d = {'wind': x, 'dens': y}
#df = pd.DataFrame(data=d)

#plt.figure(figsize=(8 * 1.618, 8))
#plt.plot(x, y, 'o', color='b', alpha=0.2)
##plt.xlabel('Total 24-hr Wind Miles (Summit)', fontsize=14)
#plt.xlabel('Total 24-hr Wind Kilometers (Summit)', fontsize=14)
#plt.ylabel('Density (Mid)', fontsize=14)
#plt.xticks(fontsize = 14)
#plt.xticks(np.arange(0., 2000., 250.))
#plt.yticks(fontsize = 14)
#plt.title('WIND: Prediction and Confidence Intervals', fontsize=16)
##plt.xlim((0, 1200))
#plt.xlim(0, (1200 * 1.618))
#plt.ylim(0, 0.25)

## points linearly spaced for predictor variable
#x1 = pd.DataFrame({'wind': np.linspace(df.wind.min(), df.wind.max(), 100)})

## 2nd order polynomial
#poly_2 = smf.ols(formula='dens ~ 1 + wind + I(wind ** 2.0)', data=df).fit()
##poly_2 = smf.ols(formula='dens ~ 1 + wind', data=df).fit()  # 1st order
##plt.plot(x1, poly_2.predict(x1), 'k-', label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared, alpha=0.9)
#plt.plot(x1, poly_2.predict(x1), 'k-', label='OLS fit (2nd order)', alpha=0.9)

#print ""
#print "-----------WIND VS DENS-----------"
#print 'POLY2 SUMMARY: %s' % poly_2.summary()

#prstd, iv_l, iv_u = wls_prediction_std(poly_2)

#from statsmodels.stats.outliers_influence import summary_table

#st, data, ss2 = summary_table(poly_2, alpha=0.05)

#fittedvalues = data[:,2]
#predict_mean_se  = data[:,3]
#predict_mean_ci_low, predict_mean_ci_upp = data[:,4:6].T
#predict_ci_low, predict_ci_upp = data[:,6:8].T

## check we got the right things
#print np.max(np.abs(poly_2.fittedvalues - fittedvalues))
#print np.max(np.abs(iv_l - predict_ci_low))
#print np.max(np.abs(iv_u - predict_ci_upp))

#data_intervals = {'wind': x, 'predict_low': predict_ci_low, 'predict_upp': predict_ci_upp, 'conf_low': predict_mean_ci_low, 'conf_high': predict_mean_ci_upp}
#df_intervals = pd.DataFrame(data=data_intervals)

#df_intervals_sort = df_intervals.sort(columns='wind')

##embed()

#plt.plot(df_intervals_sort.wind, df_intervals_sort.predict_low, color='r', linestyle='--', label='95% prediction interval')
#plt.plot(df_intervals_sort.wind, df_intervals_sort.predict_upp, color='r', linestyle='--', label='')
#plt.plot(df_intervals_sort.wind, df_intervals_sort.conf_low, color='k', linestyle='--', label='95% confidence interval')
#plt.plot(df_intervals_sort.wind, df_intervals_sort.conf_high, color='k', linestyle='--', label='')

#plt.legend(loc=2)

##plt.savefig('cipi_wind_450.png',dpi=450)
##plt.savefig('cipi_wind_450_metric.png',dpi=450)

##plt.show()
##embed()         


##-----------------------------------------------------------------------------
## WIND VS. DENSITY: QUANTILE REGRESSION
##-----------------------------------------------------------------------------

## With quantile regression:

## Least Absolute Deviation
## The LAD model is a special case of quantile regression where q=0.5

#mod = smf.quantreg('dens ~ wind + I(wind ** 2.0)', df)
#res = mod.fit(q=.5)
##print(res.summary())

## Quantile regression for 5 quantiles

#quantiles = [.05, .25, .50, .75, .95]

## get all result instances in a list
#res_all = [mod.fit(q=q) for q in quantiles]

#res_ols = smf.ols('dens ~ wind + I(wind ** 2.0)', df).fit()


#plt.figure(figsize=(8 * 1.618, 8))

## create x for prediction
#x_p = np.linspace(df.wind.min(), df.wind.max(), 50)
#df_p = pd.DataFrame({'wind': x_p})
    
#y_ols_predicted = res_ols.predict(df_p)

##plt.plot(x_p, y_ols_predicted, color='k', zorder=1, label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared )
#plt.plot(x_p, y_ols_predicted, color='k', zorder=1, label='OLS fit (2nd order)')
##plt.scatter(df.temp, df.dens, alpha=.2)
#plt.plot(df.wind, df.dens, 'o', alpha=.2, zorder=0, label='')
#plt.xlim(0, 1200)
#plt.xlim(0, (1200 * 1.618))
#plt.ylim(0, 0.25)
##plt.legend(loc="upper center")
##plt.xlabel('Total 24-hr Wind Miles (Summit)', fontsize=14)
#plt.xlabel('Total 24-hr Wind Kilometers (Summit)', fontsize=14)
#plt.ylabel('Density (Mid)', fontsize=14)
#plt.title('WIND: Quantile Regression', fontsize=16)
#plt.xticks(fontsize = 14)
#plt.xticks(np.arange(0., 2000., 250.))
#plt.yticks(fontsize = 14)

#tick=1 # tick method below to get the for loop to print only one legend label
#for qm, res in zip(quantiles, res_all):
    ## get prediction for the model and plot
    ## here we use a dict which works the same way as the df in ols
    #if tick == 1:
      #plt.plot(x_p, res.predict({'wind': x_p}), linestyle='--', lw=1, 
             #color='k', label='5th, 25th, 50th, 75th, 95th percentiles', zorder=2)
    #else:
      #plt.plot(x_p, res.predict({'wind': x_p}), linestyle='--', lw=1, 
             #color='k', label='', zorder=2)
    #tick = tick + 1

#plt.legend(loc=2)

##plt.savefig('quantile_wind_450.png',dpi=450)
##plt.savefig('quantile_wind_450_metric.png',dpi=450)

##-----------------------------------------------------------------------------
## TEMP vs DENSITY: CI and PI for 2nd order poly
##-----------------------------------------------------------------------------
## adapted from: http://stackoverflow.com/questions/34998772/plotting-confidence-and-prediction-intervals-with-repeated-entries

#x=temp_mean_mid[idx_mid.index]

## Toggle below for metrics!
#x = (x - 32.) * (5./9.)

#y=densityMid[idx_mid.index]

#d = {'temp': x, 'dens': y}
#df = pd.DataFrame(data=d)

#plt.figure(figsize=(8 * 1.618, 8))
#plt.plot(x, y, 'o', color='b', alpha=0.2)
#plt.xlabel('Mean 24-hr Air Temperature ($^\circ$C) (Mid)', fontsize=14)
#plt.ylabel('Density (Mid)', fontsize=14)
#plt.xticks(fontsize = 14)
#plt.yticks(fontsize = 14)
#plt.title('AIR TEMPERATURE: Prediction and Confidence Intervals', fontsize=16)
##plt.xlim((-20, 50))
#plt.xlim((-30, 10))
#plt.ylim((0, 0.25))

## points linearly spaced for predictor variable
#x1 = pd.DataFrame({'temp': np.linspace(df.temp.min(), df.temp.max(), 100)})

## 2nd order polynomial
#poly_2 = smf.ols(formula='dens ~ 1 + temp + I(temp ** 2.0)', data=df).fit()
##poly_2 = smf.ols(formula='dens ~ 1 + wind', data=df).fit()  # 1st order
##plt.plot(x1, poly_2.predict(x1), 'k-', label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared, alpha=0.9)
#plt.plot(x1, poly_2.predict(x1), 'k-', label='OLS fit (2nd order)', alpha=0.9)

#print ""
#print "-----------TEMP VS DENS-----------"
#print 'POLY2 SUMMARY: %s' % poly_2.summary()

#prstd, iv_l, iv_u = wls_prediction_std(poly_2)

#from statsmodels.stats.outliers_influence import summary_table

#st, data, ss2 = summary_table(poly_2, alpha=0.05)

#fittedvalues = data[:,2]
#predict_mean_se  = data[:,3]
#predict_mean_ci_low, predict_mean_ci_upp = data[:,4:6].T
#predict_ci_low, predict_ci_upp = data[:,6:8].T

## check we got the right things
#print np.max(np.abs(poly_2.fittedvalues - fittedvalues))
#print np.max(np.abs(iv_l - predict_ci_low))
#print np.max(np.abs(iv_u - predict_ci_upp))

#data_intervals = {'temp': x, 'predict_low': predict_ci_low, 'predict_upp': predict_ci_upp, 'conf_low': predict_mean_ci_low, 'conf_high': predict_mean_ci_upp}
#df_intervals = pd.DataFrame(data=data_intervals)

#df_intervals_sort = df_intervals.sort(columns='temp')

##embed()

#plt.plot(df_intervals_sort.temp, df_intervals_sort.predict_low, color='r', linestyle='--', label='95% prediction interval')
#plt.plot(df_intervals_sort.temp, df_intervals_sort.predict_upp, color='r', linestyle='--', label='')
#plt.plot(df_intervals_sort.temp, df_intervals_sort.conf_low, color='k', linestyle='--', label='95% confidence interval')
#plt.plot(df_intervals_sort.temp, df_intervals_sort.conf_high, color='k', linestyle='--', label='')

#plt.legend(loc=2)

##plt.savefig('cipi_temp.png',dpi=150)
##plt.savefig('cipi_temp_450_metric.png',dpi=450)

##plt.show()
##embed()         


##-----------------------------------------------------------------------------
## TEMP VS. DENSITY: QUANTILE REGRESSION
##-----------------------------------------------------------------------------

## With quantile regression:

## Least Absolute Deviation
## The LAD model is a special case of quantile regression where q=0.5

#mod = smf.quantreg('dens ~ temp + I(temp ** 2.0)', df)
#res = mod.fit(q=.5)
##print(res.summary())

## Quantile regression for 5 quantiles

#quantiles = [.05, .25, .50, .75, .95]

## get all result instances in a list
#res_all = [mod.fit(q=q) for q in quantiles]

#res_ols = smf.ols('dens ~ temp + I(temp ** 2.0)', df).fit()


#plt.figure(figsize=(8 * 1.618, 8))

## create x for prediction
#x_p = np.linspace(df.temp.min(), df.temp.max(), 50)
#df_p = pd.DataFrame({'temp': x_p})
    
#y_ols_predicted = res_ols.predict(df_p)

##plt.plot(x_p, y_ols_predicted, color='k', zorder=1, label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared )
#plt.plot(x_p, y_ols_predicted, color='k', zorder=1, label='OLS fit (2nd order)')
##plt.scatter(df.temp, df.dens, alpha=.2)
#plt.plot(df.temp, df.dens, 'o', alpha=.2, zorder=0, label='')
##plt.xlim((-20, 50))
#plt.xlim((-30, 10))
#plt.ylim((0, 0.25))
#plt.xlabel('Mean 24-hr Air Temperature ($^\circ$C) (Mid)', fontsize=14)
#plt.ylabel('Density (Mid)', fontsize=14)
#plt.title('AIR TEMPERATURE: Quantile Regression', fontsize=16)
#plt.xticks(fontsize = 14)
#plt.yticks(fontsize = 14)

#tick=1 # tick method below to get the for loop to print only one legend label
#for qm, res in zip(quantiles, res_all):
    ## get prediction for the model and plot
    ## here we use a dict which works the same way as the df in ols
    #if tick == 1:
      #plt.plot(x_p, res.predict({'temp': x_p}), linestyle='--', lw=1, 
             #color='k', label='5th, 25th, 50th, 75th, 95th percentiles', zorder=2)
    #else:
      #plt.plot(x_p, res.predict({'temp': x_p}), linestyle='--', lw=1, 
             #color='k', label='', zorder=2)
    #tick = tick + 1

#plt.legend(loc=2)

##plt.savefig('quantile_temp.png',dpi=150)
##plt.savefig('quantile_temp_450_metric.png',dpi=450)

##plt.show()

#-----------------------------------------------------------------------------
# TEMP VS. DENSITY: ADD WALL PLOT & CROCUS EQTN
#-----------------------------------------------------------------------------

# Define directory path:
fpath_wall='wall_chart.csv' # manual picks from wall chart photocopy

# Read in data:
walldata = pd.read_csv(fpath_wall, sep=',')
#walldata.Temp = (walldata.Temp - 32.) * (5./9.)

from scipy.signal import savgol_filter

#walldata_smooth = pd.rolling_mean(walldata, 2)
walldata_smooth = savgol_filter(walldata.Dens, 9, 3)

#embed()
#-----------------------------------------------------------------------------
plt.figure(figsize=(8 * 1.618, 8))
#plt.plot(x, y, 'o', color='b', alpha=0.2)
plt.xlabel('Mean 24-hr Air Temperature ($^\circ$F)', fontsize=14)
plt.ylabel('Density', fontsize=14)
plt.xticks(fontsize = 14)
plt.yticks(fontsize = 14)
plt.title('Density calculator vs. wall chart', fontsize=16)
plt.xlim((-20, 50))
#plt.xlim((-30, 10))
plt.ylim((0, 0.20))
plt.grid(which='major', color='k', alpha=0.5, linestyle='-')

#-----------------------------------------------------------------------------
# BOWL

x = temp_mean_Summit[idx_bowl.index]
# Toggle below for metrics!
#x = (x - 32.) * (5./9.)

y=densityBowl[idx_bowl.index]

d = {'temp': x, 'dens': y}
df = pd.DataFrame(data=d)

# points linearly spaced for predictor variable
x1 = pd.DataFrame({'temp': np.linspace(df.temp.min(), df.temp.max(), 100)})

# 2nd order polynomial
poly_2 = smf.ols(formula='dens ~ 1 + temp + I(temp ** 2.0)', data=df).fit()
#poly_2 = smf.ols(formula='dens ~ 1 + wind', data=df).fit()  # 1st order
plt.plot(x1, poly_2.predict(x1), 'm-', label='Bowl', alpha=0.9)

#-----------------------------------------------------------------------------
# RAYMER

x = temp_mean_ray[idx_bridger.index]
# Toggle below for metrics!
#x = (x - 32.) * (5./9.)

y=densityBridger[idx_bridger.index]

d = {'temp': x, 'dens': y}
df = pd.DataFrame(data=d)

# points linearly spaced for predictor variable
x1 = pd.DataFrame({'temp': np.linspace(df.temp.min(), df.temp.max(), 100)})

# 2nd order polynomial
poly_2 = smf.ols(formula='dens ~ 1 + temp + I(temp ** 2.0)', data=df).fit()
#poly_2 = smf.ols(formula='dens ~ 1 + wind', data=df).fit()  # 1st order
plt.plot(x1, poly_2.predict(x1), 'g-', label='Raymer', alpha=0.9)

#-----------------------------------------------------------------------------
# MID

x=temp_mean_mid[idx_mid.index]
#x = (x - 32.) * (5./9.)

y=densityMid[idx_mid.index]

d = {'temp': x, 'dens': y}
df = pd.DataFrame(data=d)

# points linearly spaced for predictor variable
x1 = pd.DataFrame({'temp': np.linspace(df.temp.min(), df.temp.max(), 100)})

# 2nd order polynomial
poly_2 = smf.ols(formula='dens ~ 1 + temp + I(temp ** 2.0)', data=df).fit()
#poly_2 = smf.ols(formula='dens ~ 1 + wind', data=df).fit()  # 1st order
plt.plot(x1, poly_2.predict(x1), 'b-', label='Mid', alpha=0.9)

#-----------------------------------------------------------------------------
# WALL CHART

plt.plot(walldata.Temp, walldata_smooth, 'k-', linewidth=2, label='wall chart')
#plt.plot(walldata.Temp, walldata.Dens, 'r-', label='wall chart')  

plt.legend(loc=2)

plt.savefig('wallchart_compare.png',dpi=350)

plt.show()
#embed()    
